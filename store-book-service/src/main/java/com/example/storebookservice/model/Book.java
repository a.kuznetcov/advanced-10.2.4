package com.example.storebookservice.model;

import lombok.*;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Document(collection = "books")
@ToString
public class Book {

    @Id
    private Long id;
    private String name;
    private String description;
    private String status;
    private double price;
}